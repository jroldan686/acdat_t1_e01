package jrl.acdat.acdat_t1_e01;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

public class MainActivity extends Activity implements OnClickListener {
	
	EditText cambio, euros, dolares;
	RadioButton dolaresEuros, eurosDolares;
	Button calcular;
	Toast mensaje;
	double valorCambio;
	Conversion conversor;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		cambio = (EditText)findViewById(R.id.edtCambio);
		dolares = (EditText)findViewById(R.id.edtDolares);
		euros = (EditText)findViewById(R.id.edtEuros);
		eurosDolares = (RadioButton)findViewById(R.id.rdoAEuros);
		dolaresEuros = (RadioButton)findViewById(R.id.rdoADolares);
		calcular = (Button)findViewById(R.id.btnConvertir);
		calcular.setOnClickListener(this);
	}
	
	public void onClick(View v) {
		if (v == calcular) {
			try {
				valorCambio = Double.valueOf(cambio.getText().toString());
				conversor = new Conversion(valorCambio);
			} catch (Exception e) {
				mensaje = Toast.makeText(getApplicationContext(), "Introduzca un valor en Euros para el cambio en Dolares", Toast.LENGTH_SHORT);
				mensaje.show();
			}
			if (dolaresEuros.isChecked()) {
				try {
					//String resultado = convertirADolares(euros.getText().toString(), valorCambio);
					String resultado = conversor.convertirADolares(euros.getText().toString());
					dolares.setText(resultado);
				}
				catch (Exception e) {
					mensaje = Toast.makeText(getApplicationContext(), "Introduzca una cantidad en Dolares", Toast.LENGTH_SHORT);
					mensaje.show();
				}
			}
			if (eurosDolares.isChecked()) {
				try {
					//String resultado = convertirAEuros(dolares.getText().toString(), valorCambio);
					String resultado = conversor.convertirAEuros(dolares.getText().toString());
					euros.setText(resultado);
				}
				catch (Exception e) {
					mensaje = Toast.makeText(getApplicationContext(), "Introduzca una cantidad en Euros", Toast.LENGTH_SHORT);
					mensaje.show();
				}
			}
		}
	}
	/*
	private String convertirADolares(String cantidad, double cambio) {
		double valor = Double.parseDouble(cantidad) / cambio;
		return String.format("%.2f", valor);
	}
	
	private String convertirAEuros(String cantidad, double cambio) {
		double valor = Double.parseDouble(cantidad) * cambio;
		return String.format("%.2f", valor);
	}
	*/
}